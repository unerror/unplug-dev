FROM alpine:latest

# lets update
RUN apk update

RUN apk add git py-pip alpine-sdk python3-dev
RUN git clone --branch dev https://gitlab.com/unerror/unplug.git /opt/unplug
RUN pip install -r /opt/unplug/requirements.txt

# now lets add testing stuff
#RUN apk add pytest twine build setuptools wheel virtualenv

# comment to run pipeline w/o logging in
